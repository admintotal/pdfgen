from flask import Flask, request
from flask import jsonify
from flask import make_response
from flask_cors import CORS
import time
import pytz
import os
import sys
import subprocess
import logging
from slugify import slugify

import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration
from sentry_sdk import capture_exception

logging.basicConfig(stream=sys.stdout, level=logging.INFO)

app = Flask(__name__)
APP_SETTINGS = os.environ.get("APP_SETTINGS", "settings.py")

if APP_SETTINGS:
    app.config.from_pyfile(APP_SETTINGS)
    SENTRY_DSN = app.config.get("SENTRY_DSN")
    if SENTRY_DSN:
        sentry_sdk.init(
            dsn=SENTRY_DSN,
            integrations=[FlaskIntegration()],
            send_default_pii=True,
        )

logging = app.logger
CORS(app)

USER_AGENT = (
    "Mozilla/5.0 (X11; Fedora; Linux x86_64) "
    "AppleWebKit/537.36 (KHTML, like Gecko) "
    "Chrome/79.0.3945.117 Safari/537.36"
)

# WKHTMLTOPDF SETTINGS
WK_TMP_FILE_PREFIX = app.config.get("WK_TMP_FILE_PREFIX", "__pdfgen")
WK_TMP_DIR = app.config.get("WK_TMP_DIR", "/tmp/")
WK_TMP_MAX_MINS = 25
WK_VERSIONS = {
    "0.12.1.4-2": "/opt/wkhtmltopdf/0.12.1.4-2/usr/local/bin/wkhtmltopdf",
    "0.12.5-1": "/opt/wkhtmltopdf/0.12.5-1/usr/local/bin/wkhtmltopdf",
    "0.12.6-1": "/opt/wkhtmltopdf/0.12.6-1/usr/local/bin/wkhtmltopdf",
    #'default': '0.12.5-1',
    "default": "0.12.6-1",
}
if WK_TMP_DIR.endswith("/"):
    WK_TMP_DIR = WK_TMP_DIR[:-1]

logging.info(" *****************")
logging.info(" Endpoint para creación de pdf:")
logging.info(" [POST] http://127.0.0.1:5000/api/v1/create/")
logging.info(" --- ")
logging.info(" Endpoint para creación de qr:")
logging.info(" [GET] http://127.0.0.1:5000/api/v1/qrencode/")
logging.info(" *****************")


@app.route("/test_sentry/", methods=["GET"])
def test_sentry():
    division_by_zero = 1 / 0


@app.route("/api/v1/qrencode/", methods=["GET"])
def qrencode():
    value = request.args.get("value", "")

    if not value:
        return jsonify({"status": "error", "message": "Valor inválido"}), 400

    command = ["qrencode", "-o", "-", value]

    try:
        proc = subprocess.Popen(
            command, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
    except Exception as e:
        capture_exception(e)
        return (
            jsonify(
                {
                    "status": "error",
                    "message": """Hubo un error al intentar generar el QRCODE:
            exception:%s"""
                    % (e),
                }
            ),
            500,
        )

    try:
        body_contents, errs = proc.communicate(timeout=40)
    except subprocess.TimeoutExpired as e:
        capture_exception(e)
        proc.kill()
        raise e

    response = make_response(body_contents)
    response.headers["Content-Type"] = "image/png;"
    return response


@app.route("/api/v1/create/", methods=["POST"])
def render_pdf():
    json_data = request.get_json()

    if not json_data:
        return jsonify({"status": "error", "message": "JSON inválido"}), 400

    filename = json_data.get("filename", "filename")
    version = json_data.get("version")
    html = json_data.get("html")
    opts = json_data.get("options", {})

    if not version:
        version = WK_VERSIONS.get("default")

    if not version in WK_VERSIONS:
        version = WK_VERSIONS.get("default")

    if filename.endswith(".pdf"):
        filename = filename[:-4]

    filename = slugify(filename)

    if not html:
        return (
            jsonify({"status": "error", "message": "html no especificado"}),
            400,
        )

    tmp_file = "{}/{}-{}-{}.html".format(
        WK_TMP_DIR, WK_TMP_FILE_PREFIX, filename, time.time()
    )

    with open(tmp_file, "w") as tmpf:
        tmpf.write(html)

    WKHTMLTOPDF = WK_VERSIONS.get(version)
    timeout = (60 * 20)

    timeout_command = [
        "perl",
        "-e",
        "alarm shift @ARGV; exec @ARGV",
        str(timeout), 
    ]

    command = timeout_command + [
        WKHTMLTOPDF,
        "--custom-header",
        "User-Agent",
        USER_AGENT,
        "--custom-header",
        "Connection",
        "Keep-Alive,Upgrade",
        "--custom-header-propagation",
    ]

    if opts.get("header-html"):
        command.append("--header-html")
        command.append(opts.get("header-html"))

    if opts.get("footer-html"):
        command.append("--footer-html")
        command.append(opts.get("footer-html"))

    if opts.get("header-left"):
        command.append("--header-left")
        command.append(opts.get("header-left"))

    if opts.get("header-right"):
        command.append("--header-right")
        command.append(opts.get("header-right"))

    if opts.get("footer-left"):
        command.append("--footer-left")
        command.append(opts.get("footer-left"))

    if opts.get("footer-right"):
        command.append("--footer-right")
        command.append(opts.get("footer-right"))

    if opts.get("orieintation"):
        command.append("-O")
        command.append(opts.get("orieintation"))

    if opts.get("paper-size"):
        command.append("-s")
        command.append(opts.get("paper-size"))

    if opts.get("page-width"):
        command.append("--page-width")
        command.append(opts.get("page-width"))

    if opts.get("page-height"):
        command.append("--page-height")
        command.append(opts.get("page-height"))

    if opts.get("margins") is False:
        command.append("-T")
        command.append("0")
        command.append("-B")
        command.append("0")
        command.append("-L")
        command.append("0")
        command.append("-R")
        command.append("0")

    if opts.get("header-spacing"):
        command.append("--header-spacing")
        command.append("%s" % opts.get("header-spacing"))

    if opts.get("footer-spacing"):
        command.append("--footer-spacing")
        command.append("%s" % opts.get("footer-spacing"))

    if opts.get("footer-font-size"):
        command.append("--footer-font-size")
        command.append("%s" % opts.get("footer-font-size"))

    if opts.get("margin-bottom"):
        command.append("--margin-bottom")
        command.append("%s" % opts.get("margin-bottom"))

    if opts.get("margin-top"):
        command.append("--margin-top")
        command.append("%s" % opts.get("margin-top"))

    if opts.get("disable-smart-shrinking"):
        command.append("--disable-smart-shrinking")

    command.append(tmp_file)
    command.append("-")

    try:
        print(" ".join(command))
        proc = subprocess.Popen(
            command, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )
    except Exception as e:
        capture_exception(e)
        return (
            jsonify(
                {
                    "status": "error",
                    "message": """Hubo un error al intentar generar el archivo PDF:
            exception:%s"""
                    % (e),
                }
            ),
            500,
        )

    try:
        body_contents, errs = proc.communicate(timeout=timeout)
    except subprocess.TimeoutExpired as e:
        capture_exception(e)
        proc.kill()
        raise e

    response = make_response(body_contents)
    response.headers["Content-Type"] = "application/pdf;"
    response.headers["Content-Disposition"] = (
        "inline; filename=%s.pdf" % filename
    )
    return response


app.config.update(
    SCHEDULER_API_ENABLED=True,
    JOBS=[
        {
            "id": "delete_tmp_files",
            "func": "cronjobs:delete_tmp_files",
            "args": (),
            "trigger": "interval",
            "minutes": 15,
        }
    ],
)

if __name__ == "__main__":
    from flask_apscheduler import APScheduler

    scheduler = APScheduler()
    scheduler.init_app(app)
    scheduler.start()

    app.run(debug=True)
