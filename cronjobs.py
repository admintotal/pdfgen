import os
import subprocess
import inspect
from app import logging, WK_TMP_DIR, WK_TMP_FILE_PREFIX, WK_TMP_MAX_MINS

def delete_tmp_files():
    FNULL = open(os.devnull, 'w')
    cmd = 'find {} -type f -name "{}*" -mmin +{} -exec rm {{}} \;'.format(
        WK_TMP_DIR, 
        WK_TMP_FILE_PREFIX,
        WK_TMP_MAX_MINS, 
    )
    try:
        logging.info('Eliminando archivos temporales')
        logging.debug(cmd)
        subprocess.call(
            cmd, 
            shell=True, 
            stdout=FNULL, 
            stderr=subprocess.STDOUT
        )
    except Exception as e:
        raise e

if __name__ == '__main__':
    import argparse
    import sys

    parser = argparse.ArgumentParser(description=u'Admintotal CLI util')
    parser.add_argument(
        'func', 
        help=u'Función a ejecutar'
    )

    args = parser.parse_args()
    func = getattr(sys.modules[__name__], args.func, None)
    
    if not func:
        raise Exception('La función {} no existe'.format(args.func))

    func()