#!/usr/bin/env python3
from fabric import Connection
from invoke import task

@task
def deploy(c):
	server_conn = Connection('root@pdfgen')
	virtualenv = '/srv/virtualenvs/pdfgen'
	python_pip = '{}/bin/pip'.format(virtualenv)
	python = '{}/bin/python'.format(virtualenv)
	repo = '/srv/Projects/pdfgen'

	print("Actualizando repositorio")
	server_conn.run('cd {};git pull '.format(repo))

	print("Actualizando dependencias")
	server_conn.run('{} install -q -r {}/requirements.txt'.format(python_pip, repo))

	print("Touch ini")
	server_conn.run('touch {}/reload_uwsgi'.format(repo))

	print("Finalizado.")

@task
def dev(c):
	server_conn = Connection('root@pdfgen')
	virtualenv = '/srv/virtualenvs/dev_pdfgen'
	python_pip = '{}/bin/pip'.format(virtualenv)
	python = '{}/bin/python'.format(virtualenv)
	repo = '/srv/Projects/dev_pdfgen'

	print("Actualizando repositorio")
	server_conn.run('cd {};git pull '.format(repo))

	print("Actualizando dependencias")
	server_conn.run('{} install -q -r {}/requirements.txt'.format(python_pip, repo))

	print("Touch ini")
	server_conn.run('/usr/bin/supervisorctl restart dev_pdfgen')

	print("Finalizado.")